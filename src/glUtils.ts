function getGLTypeWidthInBytes(gl: WebGL2RenderingContext, type: GLenum) {
  switch(type) {
    case gl.BYTE:
      return 1;
    case gl.SHORT:
      return 2;
    case gl.INT:
      return 4;
    case gl.UNSIGNED_INT:
      return 4;
    case gl.UNSIGNED_BYTE:
      return 1;
    case gl.UNSIGNED_SHORT:
      return 2;
    case gl.SHORT:
      return 2;
    case gl.FLOAT:
      return 4;
    case gl.FLOAT_VEC2:
      return 8;
  }
}

type WebGLAttribLocation = number;

interface Attrib {
  location: WebGLAttribLocation,
  numComponents: number,
  type: GLenum,
  stride?: number,
}

type AttribsObj = {
  [key: string]: Attrib;
};

interface Uniform {
  type: any;
  location: WebGLUniformLocation;
}

type UniformsObj = {
  [key: string]: Uniform;
};

export class Buffer {
  buffer: WebGLBuffer;
  verts: number[];
  vao: WebGLVertexArrayObject;

  constructor(gl: WebGL2RenderingContext, prog: Program) {
    this.buffer = gl.createBuffer();
    this.vao = gl.createVertexArray();
    gl.bindVertexArray(this.vao);

    gl.bindBuffer(gl.ARRAY_BUFFER, this.buffer);

    let offset = 0;
    for (let name in prog.attribs) {
      gl.vertexAttribPointer(prog.attribs[name].location,
                             prog.attribs[name].numComponents,
                             prog.attribs[name].type,
                             false,
                             prog.stride,
                             offset);
      gl.enableVertexAttribArray(prog.attribs[name].location);
      offset += prog.attribs[name].numComponents *
                getGLTypeWidthInBytes(gl, prog.attribs[name].type);
    }
  }
}

export class Program {
  glProgram: WebGLProgram;
  attribs: AttribsObj;
  uniforms: UniformsObj;
  stride: number;
  numComponents: number;

  constructor(glProgram: WebGLProgram) {
    this.glProgram = glProgram;
    this.attribs = {};
    this.uniforms = {};
    this.numComponents = 0;
    this.stride = 0;
  }

  addAttrib(gl: WebGL2RenderingContext,
            varName: string,
            numComponents: number,
            type: GLenum) {
    const attrib = {
      location: gl.getAttribLocation(this.glProgram, varName),
      numComponents,
      type,
    }
    this.attribs[varName] = attrib;
    this.numComponents += numComponents;
    this.stride += numComponents *
              getGLTypeWidthInBytes(gl, type);
  }

  addUniform(gl: WebGL2RenderingContext, varName: string, type: GLenum) {
    const uniform = {
      type,
      location: gl.getUniformLocation(this.glProgram, varName),
    };
    this.uniforms[varName] = uniform
  }

  setUniform(gl: WebGL2RenderingContext, varName: string, val: any) {
    const uniform = this.uniforms[varName];
    switch(uniform.type) {
      case gl.FLOAT:
        gl.uniform1f(uniform.location, val);
        break;
      case gl.FLOAT_VEC2:
        gl.uniform2fv(uniform.location, val);
        break;
      case gl.FLOAT_VEC3:
        gl.uniform3fv(uniform.location, val);
        break;
      case gl.FLOAT_VEC4:
        gl.uniform4fv(uniform.location, val);
        break;
      case gl.FLOAT_MAT4:
        gl.uniformMatrix4fv(uniform.location, false, val);
        break;
      case gl.BOOL:
      case gl.INT:
        gl.uniform1i(uniform.location, val);
        break;
      case gl.UNSIGNED_INT:
        gl.uniform1ui(uniform.location, val);
        break;
      default:
        console.error(`Unknown uniform type ${uniform.type}`);
    }
  }
}

export function compileShader(gl: WebGL2RenderingContext,
                              shaderSource: string,
                              shaderType: number) {
  const shader = gl.createShader(shaderType);
  gl.shaderSource(shader, shaderSource);
  gl.compileShader(shader);
  const success = gl.getShaderParameter(shader, gl.COMPILE_STATUS);
  if (!success)
    throw `Failed to compile shader: ${gl.getShaderInfoLog(shader)}`
  return shader;
}

export function createProgram(gl: WebGL2RenderingContext,
                              vertShader: WebGLShader,
                              fragShader: WebGLShader) {
  const program = gl.createProgram();
  gl.attachShader(program, vertShader);
  gl.attachShader(program, fragShader);
  gl.linkProgram(program);
  const success = gl.getProgramParameter(program, gl.LINK_STATUS);
  if (!success)
    throw `Failed to link program: ${gl.getProgramInfoLog(program)}`
  return program;
}

export function setupTexture(gl: WebGL2RenderingContext, textureObj: WebGLTexture) {
  gl.activeTexture(gl.TEXTURE0);
  gl.bindTexture(gl.TEXTURE_2D, textureObj);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);
}

export function updateTexture(gl: WebGL2RenderingContext,
                              textureObj: WebGLTexture,
                              img: TexImageSource) {
  gl.bindTexture(gl.TEXTURE_2D, textureObj);
  gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, img);
}
