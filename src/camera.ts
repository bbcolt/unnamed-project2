import {Entity} from './entity';
import {mat4, vec3} from 'gl-matrix';

export class Camera {
  entity: Entity;
  eye: vec3;
  worldUp: vec3;

  constructor(entity: Entity) {
    this.entity = entity;
    this.eye = entity.pos;
    this.worldUp = vec3.fromValues(0, 1, 0);
  }

  get yaw(): number {
    return this.entity.angle;
  }

  get pitch(): number {
    return this.entity.pitch;
  }

  get front(): vec3 {
    return this.entity.front;
  }

  get viewMatrix(): mat4 {
    return mat4.lookAt(mat4.create(),
                       this.eye,
                       vec3.add(vec3.create(),
                                this.eye,
                                this.front),
                       this.worldUp);
  }

}
