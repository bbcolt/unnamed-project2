import {Sector} from './sector';
import {Node} from './node';
import {GRID_CELL_SIZE} from './grid';
import {perpDot, intersect} from './mathUtils';
import {Entity} from './entity';
import {vec3} from 'gl-matrix';

let currSector: Sector = null;
const openSectorColor = 'yellow';
const closedSectorColor = 'purple';
const cursorColor = 'green';
let grabbedNodes: Node[] = [];
let isPanning = false;
let stickyNode = false;
let invalid = false;

export class SectorBuilder {
  sectors: Sector[] = [];
  gridOffset = {x: 0, z: 0};
  cursorPos = {x: 0, z: 0};

  constructor() {
    document.querySelector('#grid-canvas')
            .addEventListener('mousemove', (e: MouseEvent) => {
              if (isPanning) {
                this.gridOffset.x += e.movementX;
                this.gridOffset.z += e.movementY;
              }
              //const { offsetX, offsetY } = e;
              const offsetX = e.offsetX - this.gridOffset.x;
              const offsetY = e.offsetY - this.gridOffset.z;

              const cursorX = Math.round((offsetX/GRID_CELL_SIZE))*GRID_CELL_SIZE;
              const cursorZ = Math.round((offsetY/GRID_CELL_SIZE))*GRID_CELL_SIZE;
              window.debugText = `(${cursorX},${cursorZ})`;
              this.cursorPos.x = cursorX;
              this.cursorPos.z = cursorZ;

              // check for invalid wall
              if (currSector)
                invalid = !this.checkValidity(currSector.nodes.last.x, currSector.nodes.last.z,
                                        this.cursorPos.x, this.cursorPos.z);

              grabbedNodes.forEach(n => {
                const {x: oldX, z: oldZ} = n;
                n.x = this.cursorPos.x;
                n.z = this.cursorPos.z;
                coordToNodes.nodeMoved(n, oldX, oldZ);
                n.sector.isDirty = true;
              });

              /*
              window.debugCtx.clearRect(0, 0, window.debugCan.width, window.debugCan.height);
              window.debugCtx.font = '1rem arial';
              window.debugCtx.fillText(`${cursorX}, ${cursorZ}`, 0, 20);
             */
            });

    document.addEventListener('keyup', (e: KeyboardEvent) => {
      if (e.key == ' ') {
        stickyNode = false;
      }
    });

  }

  grabNodes() {
    grabbedNodes = coordToNodes.nodesAt(this.cursorPos.x, this.cursorPos.z);
  }

  releaseNodes() {
    grabbedNodes = [];
  }

  startPanning() {
    isPanning = true;
  }

  stopPanning() {
    isPanning = false;
  }

  private addNodeToSector(sector: Sector, node: Node) {
    node.sector = sector;
    sector.nodes.push(node);
  }

  private connectsHeadToTail(sector: Sector, x: number, z: number) {
    return (sector.nodes.length >= 3 &&
            sector.nodes[0].x == x && sector.nodes[0].z == z);
  }

  sectorNodesCw(sector: Sector) {
    const nodes = sector.nodes;
    let minZIdx = 0;
    let minZ = nodes[minZIdx].z;
    for (let i = 1; i < nodes.length; ++i) {
      if (nodes[i].z == minZ) {
        if (nodes[i].x > nodes[minZIdx].x) {
          minZIdx = i;
        }
      } else if (nodes[i].z < minZ) {
        minZ = nodes[i].z;
        minZIdx = i;
      }
    }

    const [v0, v1] = [nodes[minZIdx-1].x - nodes[minZIdx].x,
                      nodes[minZIdx-1].z - nodes[minZIdx].z];
    const [w0, w1] = [nodes[minZIdx+1].x - nodes[minZIdx].x,
                      nodes[minZIdx+1].z - nodes[minZIdx].z];
    return perpDot(v0, v1, w0, w1) < 0;
  }

  sectorLinkNextAndPrevNodes(sector: Sector) {
    // assumes nodes are oriented clockwise!
    const nodes = sector.nodes;
    for (let i = 0; i < nodes.length; ++i) {
      nodes[i].next = nodes[i+1];
      nodes[i].prev = nodes[i-1];
    }
  }

  // TODO: rename
  checkValidity(x0: number, z0: number, x1: number, z1: number): boolean {
    for (let j = 0; j < this.sectors.length; ++j) {
      const s = this.sectors.filter(sec => sec != currSector)[j];
      if (!s)
        continue;
      for (let j = 0; j < s.nodes.length - 1; ++j) {
        const intersection = intersect(s.nodes[j].x, s.nodes[j].z,
                                       s.nodes[j+1].x, s.nodes[j+1].z, x0, z0, x1, z1);
        if (intersection.r > 0 && intersection.r < 1 && 
            intersection.s > 0 && intersection.s < 1)
          return false;
      }
    }
    return true;
  }

  isPointOnSector(x: number, z: number, sector: Sector) {
    return sector.nodes.some(n => n.x == x && n.z == z);
  }

  // http://www.faqs.org/faqs/graphics/algorithms-faq/
  // 2.03
  isPointInSector(x: number, z: number, sector: Sector) {
    let c = false;
    const nodes = sector.nodes;
    for (let i = 0, j = nodes.length - 1; i < nodes.length; j = i++) {
      if (nodes[i].x == x && nodes[i].z == z)
        return false;
      if ( ( ((nodes[i].z <= z) && (z < nodes[j].z)) ||
             ((nodes[j].z <= z) && (z < nodes[i].z))   ) &&
           (x < (nodes[j].x - nodes[i].x) * (z - nodes[i].z) / (nodes[j].z - nodes[i].z) + nodes[i].x))
        c = !c;
    }
    return c;
  }

  shouldSplitSector(partialSector: Sector, sector: Sector): boolean {
    const nodes = partialSector.nodes;
    if (nodes.length < 2)
      return false;
    if (this.isPointOnSector(nodes[0].x, nodes[0].z, sector) &&
        this.isPointOnSector(nodes.last.x, nodes.last.z, sector)) {
      if (nodes.length == 2) {
        const midX = (nodes[0].x + nodes[1].x)/2;
        const midZ = (nodes[0].z + nodes[1].z)/2;
        return this.isPointInSector(midX, midZ, sector);
      } else {
        return this.isPointInSector(nodes[1].x, nodes[1].z, sector);
      }
    }
    return false;
  }

  sectorMarkNeighboringSectors(sector: Sector) {
    // assumes next and prev are set
    sector.nodes.forEach(n => {
      const nodesAtCurr = coordToNodes.nodesAt(n.x, n.z);
      const nodesAtNext = coordToNodes.nodesAt(n.next.x, n.next.z);
      const neighboringSectorNodes = nodesAtNext.filter(nn => nodesAtCurr.some(nnn => nnn.sector == nn.sector))
                                                .filter(nn => n.next.x == nn.x && n.next.z == nn.z &&
                                                              n.x == nn.next.x && n.z == nn.next.z);
      /*
       * I don't think this can ever be true
       * ^ WRONG; See multiple_neighboring_sectors.png
       * TODO: Confirm that having multiple neighboring sectors is valid here and
       * pick the correct one. I think I can just check which wall is closer.
       */
      if (neighboringSectorNodes.length > 1)
        console.error('multiple neighboring sector nodes?');
      else if (neighboringSectorNodes.length == 0)
        return;
      const neighboringSectorNode = neighboringSectorNodes[0];
      n.neighboringSector = neighboringSectorNode.sector;
      neighboringSectorNode.neighboringSector = n.sector;

      neighboringSectorNode.sector.isDirty = true;
    });

    const parentSector = this.sectors.filter(s => s != sector)
                                .find(s => this.isPointInSector(sector.nodes[0].x, sector.nodes[0].z, s));
    if (parentSector) {
      console.log('%cPARENT SECTOR', 'color: blue;');
      sector.parentSector = parentSector;
      parentSector.childSectors.push(sector);
      sector.nodes.forEach(n => {
        if (!n.neighboringSector) {
          n.neighboringSector = parentSector;
          // TODO: create hole in parent sector
          // might have to expand an existing hole if there are adjacent child sectors
          // Add nodes to parent sector
        }
      });
    }
  }

  placeNode() {
    // If there is no temp sector yet, create it
    if (!currSector) {
      const sector = new Sector(this.sectors.length);
      currSector = sector;
      this.sectors.push(sector);
    } 

    // Return if the cursor is on a node in the temp sector other than the 
    // first (potential closed loop)
    const sharedNodes = coordToNodes.nodesAt(this.cursorPos.x, this.cursorPos.z);
    if (currSector.nodes.length > 0 &&
        currSector.nodes.last.x == this.cursorPos.x && currSector.nodes.last.z == this.cursorPos.z)
      return;
    if (sharedNodes.filter(n => n.sector == currSector &&
                                n != currSector.nodes[0] &&
                                n.x == this.cursorPos.x && n.z == this.cursorPos.z).length > 0)
      return;

    if (this.connectsHeadToTail(currSector, this.cursorPos.x, this.cursorPos.z)) {
      if (!this.sectorNodesCw(currSector))
        currSector.nodes.reverse();
      this.sectorLinkNextAndPrevNodes(currSector);
      this.sectorMarkNeighboringSectors(currSector);
      currSector.isClosed = true;
      currSector.nodes.forEach(n => coordToNodes.addNode(n) );
      currSector.isDirty = true;
      if (currSector.parentSector)
        currSector.parentSector.isDirty = true;
      currSector = null;
      return;
    }
    if (stickyNode) {
      if (sharedNodes.length == 0 || currSector.nodes.length == 0)
        return;
    }
    stickyNode = true;
    const node = new Node(this.cursorPos.x, this.cursorPos.z);
    this.addNodeToSector(currSector, node);
    if (sharedNodes.length > 0) {
      const nodeInSectorToSplit = sharedNodes.find(n => this.shouldSplitSector(currSector, n.sector));
      if (nodeInSectorToSplit) {
        const sectorToSplit = nodeInSectorToSplit.sector;
        console.log('splitting sector');
        this.splitSector(currSector, sectorToSplit);
        currSector = null;
      }
    }
  }

  splitSector(splitter: Sector, splittee: Sector) {
    splitter.nodes.forEach(n => coordToNodes.addNode(n));
    const splitterSplitEnd = splitter.nodes.last;

    const nodesToAddToSplittee = splitter.nodes.slice(1, splitter.nodes.length - 1)
                                               .reverse()
                                               .map(n => new Node(n.x, n.z));
    // connect splitter sector head to tail and remove splittee nodes along the way
    const splitteeSplitEnd = splittee.nodes.find(n => n.x == splitterSplitEnd.x &&
                                                      n.z == splitterSplitEnd.z);
    let currNode = splitteeSplitEnd.next;
    while (!(currNode.x == splitter.nodes[0].x && currNode.z == splitter.nodes[0].z)) {
      const removeIdx = splittee.nodes.indexOf(currNode);
      splittee.nodes.splice(removeIdx, 1);
      this.addNodeToSector(splitter, currNode);
      currNode = currNode.next;
    }

    // rotate splittee
    while (!(splittee.nodes.last.x == splitterSplitEnd.x &&
             splittee.nodes.last.z == splitterSplitEnd.z))
      splittee.nodes.push(splittee.nodes.shift());
    nodesToAddToSplittee.forEach(n => {
      this.addNodeToSector(splittee, n);
      coordToNodes.addNode(n);
    });

    this.sectorLinkNextAndPrevNodes(splitter);
    this.sectorLinkNextAndPrevNodes(splittee);

    this.sectorMarkNeighboringSectors(splitter);
    this.sectorMarkNeighboringSectors(splittee);

    splitter.isClosed = true;
    splitter.isDirty = true;
    splittee.isClosed = true;
    splittee.isDirty = true;
  }

  drawCursor(ctx: CanvasRenderingContext2D) {
    ctx.beginPath();
    ctx.lineWidth = 2;
    ctx.strokeStyle = cursorColor;
    ctx.arc(this.cursorPos.x + this.gridOffset.x, this.cursorPos.z + this.gridOffset.z, 5, 0, 2*Math.PI);
    ctx.stroke();
  }

  drawSector(ctx: CanvasRenderingContext2D, sector: Sector) {
    if (sector.nodes.length > 0) {
      ctx.beginPath();
      ctx.strokeStyle = sector.isClosed ? closedSectorColor : openSectorColor;
      const [firstNode, ...rest] = sector.nodes;
      ctx.moveTo(this.gridOffset.x + firstNode.x, this.gridOffset.z + firstNode.z);
      for (let node of rest)
        ctx.lineTo(this.gridOffset.x + node.x, this.gridOffset.z + node.z)
      if (sector.isClosed)
        ctx.closePath();
      else {
        if (invalid)
          ctx.strokeStyle = 'red';
        ctx.lineTo(this.cursorPos.x + this.gridOffset.x, this.cursorPos.z + this.gridOffset.z);
      }
      ctx.stroke();
    }

    ctx.fillStyle = 'red';
    ctx.beginPath();
    sector.nodes.forEach(node => {
      ctx.moveTo(this.gridOffset.x + node.x + 5, this.gridOffset.z + node.z);
      ctx.arc(this.gridOffset.x + node.x, this.gridOffset.z + node.z, 5, 0, 2*Math.PI);
      ctx.fill();
    });

    /*
    if (window.debug) {
      ctx.beginPath();
      ctx.fillStyle = `rgb(${(sector.idx * 30 * 13) % 255}, ${(sector.idx * 5 + 100) % 255}, ${sector.idx + 123 * sector.idx}`;
      ctx.font = '15px bold Arial, serif'
      sector.nodes.forEach(node => {
        ctx.fillText(`${node.x}, ${node.z}`, node.x + 10, node.z + 10 * (sector.idx * 1.3));
      });
    }
   */
  }

  drawPlayer(ctx: CanvasRenderingContext2D, player: Entity) {
    ctx.beginPath();
    ctx.fillStyle = 'blue';
    ctx.arc(this.gridOffset.x + player.pos[0], this.gridOffset.z + player.pos[2], 5, 0, 2*Math.PI);
    ctx.fill();
    ctx.beginPath();
    ctx.strokeStyle = 'blue';
    ctx.lineWidth = 3;
    ctx.moveTo(this.gridOffset.x + player.pos[0], this.gridOffset.z + player.pos[2]);
    const forward = vec3.scale(vec3.create(), player.front, 13);
    ctx.lineTo(this.gridOffset.x + player.pos[0] + forward[0], this.gridOffset.z + player.pos[2] + forward[2]);
    ctx.stroke();
  }
}

class CoordToNodes {
  coordToNodeList: Map<string, Node[]>;

  constructor() {
    this.coordToNodeList = new Map<string, Node[]>();
  }

  addNode(node: Node) {
    const strCoord = `${node.x},${node.z}`;
    if (!this.coordToNodeList.has(strCoord))
      this.coordToNodeList.set(strCoord, []);
    this.coordToNodeList.get(strCoord).push(node);
  }

  nodesAt(x: number, z: number) {
    const coordStr = `${x},${z}`;
    return this.coordToNodeList.get(coordStr) || [];
  }

  nodeMoved(node: Node, oldX: number, oldZ: number) {
    const oldStrCoord = `${oldX},${oldZ}`;
    if (this.coordToNodeList.has(oldStrCoord)) {
      const sansMovedNode = this.coordToNodeList.get(oldStrCoord).filter(n => n != node);
      this.coordToNodeList.set(oldStrCoord, sansMovedNode);
    }
    this.addNode(node);
  }
}

const coordToNodes = new CoordToNodes();

