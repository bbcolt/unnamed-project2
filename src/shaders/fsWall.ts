export const wallFragmentShaderSource = `#version 300 es
precision mediump float;

in vec2 v_texcoord;
flat in float wallNum;
flat in int isHighlighted;
flat in int isSelected;
flat in float surfaceType;

uniform float u_time;
uniform vec2 u_mid;
uniform sampler2D u_texture;
uniform float u_sectorNum;

out vec4 outColor;

void main() {
  outColor = texture(u_texture, v_texcoord);

  if (isSelected == 1)
    outColor.rgb *= (sin(u_time * 0.05) + 3.0)/4.0;

  /*
   * [ <byte[0]>,   <byte[1]>,   <byte[2]>,   <byte[3]> ] 
   *  surface type   sector #      wall #      unused
   */
  if (abs(gl_FragCoord.x - u_mid.x) < 1.0 &&
      abs(gl_FragCoord.y - u_mid.y) < 1.0)
    outColor = vec4(surfaceType/255.0, u_sectorNum/255.0, wallNum/255.0, 1.0);
  else if (isHighlighted == 1)
    outColor.rg *= 1.4;
}
`;
