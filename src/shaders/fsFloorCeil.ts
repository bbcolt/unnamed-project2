export const floorCeilFragmentShaderSource = `#version 300 es
precision mediump float;

in vec2 v_texcoord;
flat in int isHighlighted;
flat in int isFloor;

uniform vec2 u_floorTexOffset;
uniform vec2 u_ceilTexOffset;
uniform float u_time;
uniform float u_selected;
uniform float u_sectorNum;

uniform vec2 u_mid;
uniform sampler2D u_texture;

out vec4 outColor;

void main() {
  vec2 offset = bool(isFloor) ? u_floorTexOffset : u_ceilTexOffset;
  outColor = texture(u_texture, offset + 0.25 * fract(v_texcoord));

  if (bool(isFloor) && (u_selected > -0.1) && (u_selected < 0.1))
    outColor.rgb *= (sin(u_time * 0.05) + 3.0)/4.0;
  else if (!bool(isFloor) && u_selected > 0.1)
    outColor.rgb *= (sin(u_time * 0.05) + 3.0)/4.0;

  if (abs(gl_FragCoord.x - u_mid.x) < 1.0 &&
      abs(gl_FragCoord.y - u_mid.y) < 1.0 )
    outColor = vec4((isFloor == 1 ? 3.0 : 4.0)/255.0,
                     u_sectorNum/255.0,
                     1.0,
                     1.0);
  else if (isHighlighted == 1)
    outColor.rg *= 1.4;
}
`;
