export const floorCeilVertexShaderSource = `#version 300 es
precision mediump float;

in vec3 a_position;

uniform mat4 u_matrix;
uniform float u_sectorNum;
uniform float u_focusedSectorNum;
uniform float u_focusedSurfaceType;
uniform int u_floorVertCount;

out vec2 v_texcoord;
flat out int isHighlighted;
flat out int isFloor;

void main() {
  isFloor = gl_VertexID < u_floorVertCount ? 1 : 0;

  gl_Position = u_matrix*vec4(a_position, 1.0);

  float s = a_position.x;
  float t = a_position.z;
  float scale = 100.0;
  v_texcoord = vec2(s, t) / scale;

  isHighlighted = 0;
  if (abs(u_focusedSectorNum - u_sectorNum) < 0.1) {
    // TODO: check whether current vertex is part of floor or ceil
    if ( ((abs(u_focusedSurfaceType - 3.0) < 0.1) && isFloor == 1) ||
         ((abs(u_focusedSurfaceType - 4.0) < 0.1) && isFloor == 0) )
      isHighlighted = 1;
  }
}
`;
