export const wallVertexShaderSource = `#version 300 es
precision mediump float;

in vec3 a_position;
in vec2 a_texcoord;
in float a_wallNum;
in float a_selected;
in float a_surfaceType;
uniform mat4 u_matrix;
uniform float u_focusedSectorNum;
uniform float u_focusedNodeNum;
uniform float u_focusedSurfaceType;
uniform float u_sectorNum;

out vec2 v_texcoord;
flat out float wallNum;
flat out int isHighlighted;
flat out int isSelected;
flat out float surfaceType;

void main() {
  gl_Position = u_matrix*vec4(a_position, 1.0);

  v_texcoord = a_texcoord;

  wallNum = a_wallNum;

  isHighlighted = 0;
  if (u_focusedSurfaceType < 3.0)
    if (abs(u_focusedSectorNum - u_sectorNum) < 0.1)
      if (abs(u_focusedNodeNum - a_wallNum) < 0.1)
        isHighlighted = 1;

  isSelected = a_selected > 0.1 ? 1 : 0;
  surfaceType = a_surfaceType;
}
`;
