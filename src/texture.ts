import {updateTexture} from './glUtils';

export const TEXTURE_ATLAS_DIM = 4;

export class TexturePicker {
  gl: WebGL2RenderingContext;
  tex: WebGLTexture;
  numTextures: number;
  texAtlasCtx: CanvasRenderingContext2D;
  texBufferCtx: CanvasRenderingContext2D;
  texPickerContainer: HTMLElement;
  selectedTexture: HTMLImageElement;
  selectedTexNum: number;

  constructor(gl: WebGL2RenderingContext,
              tex: WebGLTexture,
              texAtlasCtx: CanvasRenderingContext2D,
              texBufferCtx: CanvasRenderingContext2D,
              texPickerContainer: HTMLElement) {
    this.gl = gl;
    this.tex = tex;
    this.numTextures = 0;
    this.texAtlasCtx = texAtlasCtx;
    this.texBufferCtx = texBufferCtx;
    this.texPickerContainer = texPickerContainer;
    this.selectedTexture = null;
    this.selectedTexNum = null;
  }

  onImageLoaded(e: Event) {
    const img = e.target as CanvasImageSource;
    this.texBufferCtx.drawImage(img,
                                0, 0,
                                this.texBufferCtx.canvas.width,
                                this.texBufferCtx.canvas.height);
    const y = Math.floor(this.numTextures / TEXTURE_ATLAS_DIM);
    const x = (this.numTextures % TEXTURE_ATLAS_DIM);
    this.texAtlasCtx.drawImage(this.texBufferCtx.canvas,
                               x * this.texAtlasCtx.canvas.width / TEXTURE_ATLAS_DIM,
                               y * this.texAtlasCtx.canvas.height / TEXTURE_ATLAS_DIM,
                               this.texAtlasCtx.canvas.width / TEXTURE_ATLAS_DIM,
                               this.texAtlasCtx.canvas.height / TEXTURE_ATLAS_DIM);
    const texNum = this.numTextures;
    const texPickerImg = new Image();
    texPickerImg.classList.add('texture');
    texPickerImg.dataset.texNum = texNum.toString();
    texPickerImg.addEventListener('click', () => {
      document.querySelectorAll('.texture').forEach((tEl) => {
        tEl.classList.remove('selected');
      });
      this.selectedTexture = texPickerImg;
      this.selectedTexNum = parseInt(texPickerImg.dataset.texNum);
      texPickerImg.classList.add('selected');
    });
    texPickerImg.src = this.texBufferCtx.canvas.toDataURL();
    if (texNum > 0)
      this.texPickerContainer.appendChild(texPickerImg);

    updateTexture(this.gl, this.tex, this.texAtlasCtx.canvas);

    ++this.numTextures;
  }

  uploadTexture(file: File | Blob) {
    function onFileLoaded(e: Event & { target: { result: string } }) {
      const result = e.target.result;
      const img = new Image();
      img.addEventListener('load', this.onImageLoaded.bind(this));
      img.src = result;

    }

    const fr = new FileReader();
    fr.readAsDataURL(file);
    fr.addEventListener('load', onFileLoaded.bind(this));
  }

}


