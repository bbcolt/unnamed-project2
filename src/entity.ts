import {vec3} from 'gl-matrix';

export class Entity {
  pos: vec3;
  angle: number;
  pitch: number;
  speed: number;
  yawSpeed: number;

  constructor(pos = vec3.fromValues(100, 180, 200), angle = 0, pitch = -.1) {
    this.pos = pos;
    this.angle = angle;
    this.pitch = pitch;
    this.speed = 5;
    this.yawSpeed = 0.05;
  }

  get front(): vec3 {
    return vec3.fromValues(Math.cos(this.angle)*Math.cos(this.pitch),
                           Math.sin(this.pitch),
                           Math.sin(this.angle)*Math.cos(this.pitch));
  }

  moveForward(dir: number) {
    this.pos[0] +=  Math.cos(this.angle)*dir*this.speed;
    this.pos[2] +=  Math.sin(this.angle)*dir*this.speed;
  }

  moveRight(dir: number) {
    this.pos[0] -=  Math.sin(this.angle)*dir*this.speed;
    this.pos[2] +=  Math.cos(this.angle)*dir*this.speed;
  }

  moveUp(dir: number) {
    this.pos[1] += 5*dir;
  }

  rotate(x: number, z: number) {
    this.angle -= this.yawSpeed*-x;
    this.pitch -= this.yawSpeed*z;
  }
}
