import './styles.less';
import {drawGrid} from './grid';
import {SectorBuilder} from './sectorBuilder';
import {setupTexture} from './glUtils';
import {Renderer, getPixelUnderReticle} from './renderer';
import {Camera} from './camera';
import {Entity} from './entity';
import defaultTexPath from './assets/uvtexture.png';
import {InputManager, InputType, Command} from './input';
import {Sector} from './sector';
import {drawReticle} from './reticle';
import {generateWallVertsFromSector, generateFloorCeilVertsFromSector} from './glSector';
import {pixelToSurface, surfaceToDebugString, SurfaceType} from './surface';
import * as editor from './editor';
import {TexturePicker} from './texture';

declare global {
  interface Window {
    debug: boolean;
    debugText: string;
    debugCan: HTMLCanvasElement;
    debugCtx: CanvasRenderingContext2D;
  }
}
window.debug = true;
window.debugCan = document.querySelector('#debug-canvas');
window.debugCtx = window.debugCan.getContext('2d');

window.addEventListener('keydown', e => {
  if (e.key == 'p') {
    window.debug = !window.debug;
    toggleDebugCanvas();
  }
});

function toggleDebugCanvas() {
  window.debugCan.style.display = window.debugCan.style.display == 'none' ? 'block' : 'none';
}

const gridCan: HTMLCanvasElement = document.querySelector('#grid-canvas');
const mapPanel = document.querySelector('#map-panel');
gridCan.width = mapPanel.clientWidth;
gridCan.height = mapPanel.clientHeight;
const patternCan: HTMLCanvasElement = document.querySelector('#pattern-canvas');
const patternCtx = patternCan.getContext('2d');
const gridCtx = gridCan.getContext('2d');

const viewPanel: HTMLElement = document.querySelector('#view-panel');
const viewCan: HTMLCanvasElement = document.querySelector('#view-canvas');
viewCan.width = viewPanel.clientWidth;
viewCan.height = viewPanel.clientHeight;
const reticleCan: HTMLCanvasElement = document.querySelector('#reticle-canvas');
reticleCan.width = viewPanel.clientWidth;
reticleCan.height = viewPanel.clientHeight;
const reticleCtx = reticleCan.getContext('2d', { alpha: true });
drawReticle(reticleCtx);
const gl = viewCan.getContext('webgl2');
if (!gl)
  alert('Your browser does not support WebGL2');

const texAtlasCanvas: HTMLCanvasElement = document.querySelector('#texture-atlas-canvas');
const texAtlasCtx = texAtlasCanvas.getContext('2d');
const texBufferCanvas: HTMLCanvasElement = document.querySelector('#texture-buffer-canvas');
const texBufferCtx = texBufferCanvas.getContext('2d');
const texPickerContainer: HTMLElement = document.querySelector('#texture-picker-container');

const tex: WebGLTexture = gl.createTexture();
setupTexture(gl, tex);

const texturePicker = new TexturePicker(gl, tex, texAtlasCtx, texBufferCtx, texPickerContainer);
const imageUpload: HTMLInputElement = document.querySelector('#image-upload');
imageUpload.addEventListener('change', (e: Event) => {
  const files = imageUpload.files;
  [...files].forEach(file => texturePicker.uploadTexture(file));
});

const defaultTexImg = new Image();
defaultTexImg.addEventListener('load', texturePicker.onImageLoaded.bind(texturePicker));
defaultTexImg.src = defaultTexPath;

const player = new Entity();
const camera = new Camera(player);
const renderer = new Renderer(gl);

function updateDirtySector(sector: Sector) {
  const wallVerts = generateWallVertsFromSector(sector);
  const floorCeilVerts = generateFloorCeilVertsFromSector(sector);
  renderer.updateSectorWallVerts(gl, sector, wallVerts);
  renderer.updateSectorFloorCeilVerts(gl, sector, floorCeilVerts);
  sector.isDirty = false;
}

const input = new InputManager();

const sb = new SectorBuilder();

const debugCmd = new Command(() => {
  console.log(sb.sectors.map(s => s.nodes));
});
input.registerCommand(debugCmd, InputType.KEY_X);

const setTextureCmd = new Command(() => {
  const selectedTexNum = texturePicker.selectedTexNum;
  editor.selectedSurfaces.forEach(surface => {
    switch(surface.type) {
      case SurfaceType.WALL:
        surface.sector.nodes[surface.nodeNum].wallTexNum = selectedTexNum;
        break;
      case SurfaceType.LOWER_WALL:
        surface.sector.nodes[surface.nodeNum].lowerWallTexNum = selectedTexNum;
        break;
      case SurfaceType.UPPER_WALL:
        surface.sector.nodes[surface.nodeNum].upperWallTexNum = selectedTexNum;
        break;
      case SurfaceType.FLOOR:
        surface.sector.floorTexNum = selectedTexNum;
        break;
      case SurfaceType.CEIL:
        surface.sector.ceilTexNum = selectedTexNum;
        break;
    }
    surface.sector.isDirty = true;
  });
});
input.registerCommand(setTextureCmd, InputType.KEY_T);

const moveForwardCmd = new Command(() => player.moveForward(1));
input.registerCommand(moveForwardCmd, InputType.KEY_W, {repeatOnHold: true});

const moveBackwardCmd = new Command(() => player.moveForward(-1));
input.registerCommand(moveBackwardCmd, InputType.KEY_S, {repeatOnHold: true});

const moveLeftCmd = new Command(() => player.moveRight(-1));
input.registerCommand(moveLeftCmd, InputType.KEY_A, {repeatOnHold: true});

const moveRightCmd = new Command(() => player.moveRight(1));
input.registerCommand(moveRightCmd, InputType.KEY_D, {repeatOnHold: true});

const moveUpCmd = new Command(() => player.moveUp(1));
input.registerCommand(moveUpCmd, InputType.KEY_E, {repeatOnHold: true});

const moveDownCmd = new Command(() => player.moveUp(-1));
input.registerCommand(moveDownCmd, InputType.KEY_Q, {repeatOnHold: true});

const rotateUpCmd = new Command(() => player.rotate(0, -1));
input.registerCommand(rotateUpCmd, InputType.KEY_ARROW_UP, {repeatOnHold: true});

const rotateDownCmd = new Command(() => player.rotate(0, 1));
input.registerCommand(rotateDownCmd, InputType.KEY_ARROW_DOWN, {repeatOnHold: true});

const rotateLeftCmd = new Command(() => player.rotate(-1, 0));
input.registerCommand(rotateLeftCmd, InputType.KEY_ARROW_LEFT, {repeatOnHold: true});

const rotateRightCmd = new Command(() => player.rotate(1, 0));
input.registerCommand(rotateRightCmd, InputType.KEY_ARROW_RIGHT, {repeatOnHold: true});

const mouseRotateCmd = new Command(inputState => {
  player.rotate(inputState.deltaX * 0.1, inputState.deltaY * 0.1);
});
input.registerCommand(mouseRotateCmd, InputType.MOUSE_MOVE, {pointerLockOnly: true});

const requestPointerLockCmd = new Command(() => {
  if (!document.pointerLockElement)
    reticleCan.requestPointerLock();
});
input.registerCommand(requestPointerLockCmd, InputType.MOUSE_LEFT_DOWN, {target: reticleCan});

const selectSurfaceCmd = new Command(inputState => {
  if (editor.selectedSurfaces.includes(editor.focusedSurface))
    editor.unselectSurface(editor.focusedSurface);
  else
    editor.selectSurface(editor.focusedSurface, !inputState.shift);
});
input.registerCommand(selectSurfaceCmd, InputType.MOUSE_LEFT_DOWN, {pointerLockOnly: true});

const raiseSelectedFloorCeilsCmd = new Command(() => {
  const selectedFloorCeils = editor.selectedSurfaces.filter(s =>
                              s.type == SurfaceType.FLOOR || s.type == SurfaceType.CEIL);
  selectedFloorCeils.forEach(fs => editor.raiseFloorCeil(fs, 2));
});
input.registerCommand(raiseSelectedFloorCeilsCmd, InputType.MOUSE_SCROLL_UP, {pointerLockOnly: true});

const lowerSelectedFloorCeilsCmd = new Command(() => {
  const selectedFloorCeils = editor.selectedSurfaces.filter(s =>
                              s.type == SurfaceType.FLOOR || s.type == SurfaceType.CEIL);
  selectedFloorCeils.forEach(fs => editor.raiseFloorCeil(fs, -2));
});
input.registerCommand(lowerSelectedFloorCeilsCmd, InputType.MOUSE_SCROLL_DOWN, {pointerLockOnly: true});

// SECTOR BUILDER COMMANDS
const placeNodeCmd = new Command(() => sb.placeNode());
input.registerCommand(placeNodeCmd, InputType.KEY_SPACE);

const grabNodesCmd = new Command(() => sb.grabNodes());
input.registerCommand(grabNodesCmd, InputType.MOUSE_LEFT_DOWN, {target: gridCan});

const releaseNodesCmd = new Command(() => sb.releaseNodes());
input.registerCommand(releaseNodesCmd, InputType.MOUSE_LEFT_UP, {target: gridCan});

const startPanningCmd = new Command(() => sb.startPanning());
input.registerCommand(startPanningCmd, InputType.MOUSE_RIGHT_DOWN, {target: gridCan});

const stopPanningCmd = new Command(() => sb.stopPanning());
input.registerCommand(stopPanningCmd, InputType.MOUSE_RIGHT_UP, {target: gridCan});

(function loop() {
  input.handleInput();

  gridCtx.clearRect(0, 0, gridCtx.canvas.width, gridCtx.canvas.height);

  drawGrid(patternCtx, gridCtx, sb.gridOffset.x, sb.gridOffset.z);

  sb.sectors.forEach(sec => {
    if (sec.isDirty)
      updateDirtySector(sec);
    sb.drawSector(gridCtx, sec);
  });
  sb.drawCursor(gridCtx);
  sb.drawPlayer(gridCtx, player);

  renderer.render(gl, camera, sb.sectors);

  const pixel = getPixelUnderReticle(gl);
  editor.setFocusedSurface(pixelToSurface(pixel, sb.sectors));
  window.debugText = surfaceToDebugString(editor.focusedSurface);

  requestAnimationFrame(loop);
})();
