export interface InputState {
  x: number,
  y: number,
  deltaX: number,
  deltaY: number,
  scrollAmt: number,
  shift: boolean;
  ctrl: boolean;
  alt: boolean;
}

export class Command {
  callback: (inputState: InputState) => void;

  constructor(callback: (inputState: InputState) => void) {
    this.callback = callback;
  }

  execute(inputState: InputState) {
    this.callback(inputState);
  }
}

export enum InputType {
  MOUSE_MOVE = 'mouse_move',
  MOUSE_LEFT_DOWN = 'mouse_left_down',
  MOUSE_LEFT_UP = 'mouse_left_up',
  MOUSE_MIDDLE_DOWN = 'mouse_middle_down',
  MOUSE_MIDDLE_UP = 'mouse_middle_up',
  MOUSE_RIGHT_DOWN = 'mouse_right_down',
  MOUSE_RIGHT_UP = 'mouse_right_up',
  MOUSE_SCROLL_UP = 'mouse_scroll_up',
  MOUSE_SCROLL_DOWN = 'mouse_scroll_down',

  KEY_A = 'a',
  KEY_B = 'b',
  KEY_C = 'c',
  KEY_D = 'd',
  KEY_E = 'e',
  KEY_F = 'f',
  KEY_G = 'g',
  KEY_H = 'h',
  KEY_I = 'i',
  KEY_J = 'j',
  KEY_K = 'k',
  KEY_L = 'l',
  KEY_M = 'm',
  KEY_N = 'n',
  KEY_O = 'o',
  KEY_P = 'p',
  KEY_Q = 'q',
  KEY_R = 'r',
  KEY_S = 's',
  KEY_T = 't',
  KEY_U = 'u',
  KEY_V = 'v',
  KEY_W = 'w',
  KEY_X = 'x',
  KEY_Y = 'y',
  KEY_Z = 'z',
  KEY_SPACE = ' ',
  KEY_ARROW_UP = 'arrowup',
  KEY_ARROW_DOWN = 'arrowdown',
  KEY_ARROW_LEFT = 'arrowleft',
  KEY_ARROW_RIGHT = 'arrowright',
}

enum ModKey {
  SHIFT = 'shift',
  CTRL = 'control',
  ALT = 'alt',
}

interface Trigger {
  command?: Command,
  repeatOnHold: boolean,
  pointerLockOnly: boolean,
  target?: EventTarget,
  isDirty: boolean,
}

export class InputManager {
  inputState: InputState;
  triggers: {[k: string]: Trigger[]};

  constructor() {
    this.inputState = {
      shift: false,
      ctrl: false,
      alt: false,
      x: 0,
      y: 0,
      deltaX: 0,
      deltaY: 0,
      scrollAmt: 0,
    };
    this.triggers = {};

    document.addEventListener('mousemove', (e: MouseEvent) => {
      this.inputState.deltaX = e.movementX;
      this.inputState.deltaY = e.movementY;
      this.inputState.x = e.clientX;
      this.inputState.y = e.clientY;
      this.setTriggers(InputType.MOUSE_MOVE, true, e.target);
    });

    document.addEventListener('mousedown', (e: MouseEvent) => {
      switch(e.button) {
        case 0:
          this.setTriggers(InputType.MOUSE_LEFT_DOWN, true, e.target);
          break;
        case 1:
          this.setTriggers(InputType.MOUSE_MIDDLE_DOWN, true, e.target);
          break;
        case 2:
          this.setTriggers(InputType.MOUSE_RIGHT_DOWN, true, e.target);
          break;
      }
    });

    document.addEventListener('mouseup', (e: MouseEvent) => {
      switch(e.button) {
        case 0:
          this.setTriggers(InputType.MOUSE_LEFT_UP, true, e.target);
          this.setTriggers(InputType.MOUSE_LEFT_DOWN, false, e.target);
          break;
        case 1:
          this.setTriggers(InputType.MOUSE_MIDDLE_UP, true, e.target);
          this.setTriggers(InputType.MOUSE_MIDDLE_DOWN, false, e.target);
          break;
        case 2:
          this.setTriggers(InputType.MOUSE_RIGHT_UP, true, e.target);
          this.setTriggers(InputType.MOUSE_RIGHT_DOWN, false, e.target);
          break;
      }
    });

    document.addEventListener('keydown', (e: KeyboardEvent) => {
      const key = e.key.toLowerCase();
      if (key == ModKey.SHIFT) {
        this.inputState.shift = true;
        return;
      } else if (key == ModKey.CTRL) {
        this.inputState.ctrl = true;
        return;
      } else if (key == ModKey.ALT) {
        this.inputState.alt = true;
        return;
      } else
        this.setTriggers(key as InputType, true, e.target);
    });

    document.addEventListener('keyup', (e: KeyboardEvent) => {
      const key = e.key.toLowerCase();
      if (key == ModKey.SHIFT) {
        this.inputState.shift = false;
        return;
      } else if (key == ModKey.CTRL) {
        this.inputState.ctrl = false;
        return;
      } else if (key == ModKey.ALT) {
        this.inputState.alt = false;
        return;
      } else
        this.setTriggers(key as InputType, false, e.target);
    });

    document.addEventListener('wheel', (e: WheelEvent) => {
      if (e.deltaY == 0)
        return;
      if (e.deltaY < 0)
        this.setTriggers(InputType.MOUSE_SCROLL_UP, true, e.target);
      else
        this.setTriggers(InputType.MOUSE_SCROLL_DOWN, true, e.target);
      this.inputState.scrollAmt = e.deltaY;
    });
  }

  setTriggers(inputType: InputType, dirty: boolean, target: EventTarget) {
    if (!this.triggers[inputType])
      return;
    this.triggers[inputType].forEach(t => {
      if (t.pointerLockOnly && !document.pointerLockElement)
        return;
      if (t.target && (t.target != target))
        return;
      t.isDirty = dirty;
    });
  }

  registerCommand(command: Command,
                  inputType: InputType,
                  {
                    repeatOnHold = false,
                    pointerLockOnly = false,
                    target = undefined as EventTarget,
                  } = {}) {
    const trigger = {
      command,
      repeatOnHold,
      pointerLockOnly,
      target,
      isDirty: false,
    };
    if (inputType in this.triggers)
      this.triggers[inputType].push(trigger);
    else
      this.triggers[inputType] = [trigger];
  }

  handleInput() {
    for (const trigger in this.triggers) {
      for (const t of this.triggers[trigger]) {
        if (!t.isDirty)
          continue;
        t.command!.execute(this.inputState);
        if (!t.repeatOnHold)
          t.isDirty = false;
      }
    }
  }
}
