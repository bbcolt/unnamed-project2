export const GRID_CELL_SIZE = 20;

export function drawGrid(patternCtx: CanvasRenderingContext2D,
                         gridCtx: CanvasRenderingContext2D,
                         offsetX: number, offsetZ: number) {
  patternCtx.canvas.width = GRID_CELL_SIZE;
  patternCtx.canvas.height = GRID_CELL_SIZE;
  patternCtx.clearRect(0, 0, patternCtx.canvas.width, patternCtx.canvas.height);
  patternCtx.lineWidth = 0.5;
  patternCtx.beginPath();

  offsetX %= GRID_CELL_SIZE;
  offsetZ %= GRID_CELL_SIZE;
  if (offsetX < 0)
    offsetX = GRID_CELL_SIZE - Math.abs(offsetX);
  if (offsetZ < 0)
    offsetZ = GRID_CELL_SIZE - Math.abs(offsetZ);

  // vertical line
  patternCtx.moveTo(offsetX, 0);
  patternCtx.lineTo(offsetX, GRID_CELL_SIZE);

  // horizontal line
  patternCtx.moveTo(0, offsetZ);
  patternCtx.lineTo(GRID_CELL_SIZE, offsetZ);

  patternCtx.stroke();

  const pattern = gridCtx.createPattern(patternCtx.canvas, 'repeat');
  gridCtx.beginPath();
  gridCtx.fillStyle = pattern;
  gridCtx.fillRect(0, 0, gridCtx.canvas.width, gridCtx.canvas.height);
}
