export const wraparoundArrayProxy = (arr: Array<any>) =>
  new Proxy(arr, {
    get(target, prop: any) {
      if (typeof prop === 'string' && prop === 'last')
        return target[target.length - 1];
      if (typeof prop != 'symbol' && !isNaN(prop))
        prop = ((parseInt(prop, 10) % target.length) + target.length) % target.length;
      return target[prop];
    }
  });
