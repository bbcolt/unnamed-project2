import {Sector} from './sector';

export class Node {
  x: number;
  z: number;
  prev?: Node;
  next?: Node;
  sector?: Sector;
  neighboringSector?: Sector;
  wallTexNum: number;
  lowerWallTexNum: number;
  upperWallTexNum: number;
  isSelected: boolean;

  constructor(x: number, z: number) {
    this.x = x;
    this.z = z;
    this.prev = null;
    this.next = null;
    this.sector = null;
    this.neighboringSector = null;
    this.wallTexNum = 0;
    this.lowerWallTexNum = 0;
    this.upperWallTexNum = 0;
    this.isSelected = false;
  }
}
