import {Node} from './node';
import {wraparoundArrayProxy} from './util';

const DEFAULT_CEIL_HEIGHT = 80;
const DEFAULT_FLOOR_HEIGHT = 0

export class Sector {
  nodes: Node[] & {last: Node};
  floorHeight: number;
  ceilHeight: number;
  floorTexNum: number;
  ceilTexNum: number;
  isClosed: boolean;
  // I think this should be a hash ID, not an index because I might run into
  // problems when I start deleting sectors
  idx: number;
  isDirty: boolean;
  isFloorSelected: boolean;
  isCeilSelected: boolean;
  childSectors: Sector[];
  parentSector: Sector;

  constructor(idx: number) {
    this.nodes = wraparoundArrayProxy([]) as Node[] & {last: Node};
    this.floorHeight = DEFAULT_FLOOR_HEIGHT;
    this.ceilHeight = DEFAULT_CEIL_HEIGHT;
    this.floorTexNum = 0;
    this.ceilTexNum = 0;
    this.isClosed = false;
    this.idx = idx;
    this.isDirty = false;
    this.isFloorSelected = false;
    this.isCeilSelected = false;
    this.childSectors = [];
    this.parentSector = null;
  }
}
