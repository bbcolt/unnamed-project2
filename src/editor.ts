import {Surface, SurfaceType} from './surface';

export let selectedSurfaces = [] as Surface[];

function setSurfaceTexture(surface: Surface, texNum: number) {
  surface.sector.nodes[surface.nodeNum].wallTexNum = texNum;
}

export function setSelectedSurfacesTexture(texNum: number) {
  selectedSurfaces.forEach(s => setSurfaceTexture(s, texNum));
}

export function selectSurface(surface: Surface, replace: boolean) {
  if (replace) {
    selectedSurfaces.forEach(s => unselectSurface(s));
    selectedSurfaces = [surface];
  } else
    selectedSurfaces.push(surface);
  switch (surface.type) {
    case SurfaceType.WALL:
    case SurfaceType.LOWER_WALL:
    case SurfaceType.UPPER_WALL:
      surface.sector.nodes[surface.nodeNum].isSelected = true;
      break;
    case SurfaceType.FLOOR:
      surface.sector.isFloorSelected = true;
      break;
    case SurfaceType.CEIL:
      surface.sector.isCeilSelected = true;
      break;
  }
  surface.sector.isDirty = true;
}

export function unselectSurface(surface: Surface) {
  switch (surface.type) {
    case SurfaceType.WALL:
    case SurfaceType.LOWER_WALL:
    case SurfaceType.UPPER_WALL:
      surface.sector.nodes[surface.nodeNum].isSelected = false;
      break;
    case SurfaceType.FLOOR:
      surface.sector.isFloorSelected = false;
      break;
    case SurfaceType.CEIL:
      surface.sector.isCeilSelected = false;
      break;
  }
  selectedSurfaces = selectedSurfaces.filter(s => s != surface);
  surface.sector.isDirty = true;
}

export function multiSelectSurface(surface: Surface) {
  selectedSurfaces.push(surface);
}

export let focusedSurface: Surface = null;

export function setFocusedSurface(surface: Surface) {
  focusedSurface = surface;
}

export function raiseFloorCeil(floorCeil: Surface, dir: number) {
  if (floorCeil.type == SurfaceType.FLOOR)
    floorCeil.sector.floorHeight += dir;
  else
    floorCeil.sector.ceilHeight += dir;
  // This is pretty expensive
  floorCeil.sector.nodes.forEach(n => {
    if (n.neighboringSector)
      n.neighboringSector.isDirty = true;
  });
  if (floorCeil.sector.childSectors.length > 0)
    floorCeil.sector.childSectors.forEach(cs => cs.isDirty = true);
  floorCeil.sector.isDirty = true;
}
