export function drawReticle(ctx: CanvasRenderingContext2D) {
  const reticleSize = 6;
  const reticleThickness = 2;
  ctx.lineWidth = reticleThickness;
  ctx.strokeStyle = 'red';
  ctx.beginPath();
  ctx.moveTo(ctx.canvas.width/2, ctx.canvas.height/2 - reticleSize);
  ctx.lineTo(ctx.canvas.width/2, ctx.canvas.height/2 + reticleSize);
  ctx.stroke();
  ctx.beginPath();
  ctx.moveTo(ctx.canvas.width/2 - reticleSize, ctx.canvas.height/2);
  ctx.lineTo(ctx.canvas.width/2 + reticleSize, ctx.canvas.height/2);
  ctx.stroke();
}
