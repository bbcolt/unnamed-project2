import {Sector} from './sector';
import {TEXTURE_ATLAS_DIM} from './texture';
import {SurfaceType} from './surface';
import earcut from 'earcut';

export function generateWallVertsFromSector(sector: Sector): number[] {
  return sector.nodes.flatMap((n, i) => {
    const texX = (n.wallTexNum % TEXTURE_ATLAS_DIM) / TEXTURE_ATLAS_DIM;
    const texY = Math.floor(n.wallTexNum / TEXTURE_ATLAS_DIM) / TEXTURE_ATLAS_DIM;
    const lowerWallTexX = (n.lowerWallTexNum % TEXTURE_ATLAS_DIM) / TEXTURE_ATLAS_DIM;
    const lowerWallTexY = Math.floor(n.lowerWallTexNum / TEXTURE_ATLAS_DIM) / TEXTURE_ATLAS_DIM;
    const upperWallTexX = (n.upperWallTexNum % TEXTURE_ATLAS_DIM) / TEXTURE_ATLAS_DIM;
    const upperWallTexY = Math.floor(n.upperWallTexNum / TEXTURE_ATLAS_DIM) / TEXTURE_ATLAS_DIM;
    const isSelected = n.isSelected ? 1 : 0;
    if (n.neighboringSector) {
      const lowerWallVerts = n.neighboringSector.floorHeight > sector.floorHeight &&
                             n.neighboringSector != sector.parentSector ? [] : [
        n.x, n.neighboringSector.floorHeight, n.z,          // x y z
        lowerWallTexX, lowerWallTexY + 1/TEXTURE_ATLAS_DIM, // u v
        i,                                                  // node #
        isSelected,                                         // selected
        SurfaceType.LOWER_WALL,                             // surface type
        n.x, sector.floorHeight, n.z,
        lowerWallTexX, lowerWallTexY,
        i,
        isSelected,
        SurfaceType.LOWER_WALL,
        n.next.x, n.neighboringSector.floorHeight, n.next.z,
        lowerWallTexX + 1/TEXTURE_ATLAS_DIM, lowerWallTexY + 1/TEXTURE_ATLAS_DIM,
        i,
        isSelected,
        SurfaceType.LOWER_WALL,
        n.next.x, n.neighboringSector.floorHeight, n.next.z,
        lowerWallTexX + 1/TEXTURE_ATLAS_DIM, lowerWallTexY + 1/TEXTURE_ATLAS_DIM,
        i,
        isSelected,
        SurfaceType.LOWER_WALL,
        n.x, sector.floorHeight, n.z,
        lowerWallTexX, lowerWallTexY,
        i,
        isSelected,
        SurfaceType.LOWER_WALL,
        n.next.x, sector.floorHeight, n.next.z,
        lowerWallTexX + 1/TEXTURE_ATLAS_DIM, lowerWallTexY,
        i,
        isSelected,
        SurfaceType.LOWER_WALL,
      ];

      const upperWallVerts = n.neighboringSector.ceilHeight > sector.ceilHeight &&
                             n.neighboringSector != sector.parentSector ? [] : [
        n.x, n.neighboringSector.ceilHeight, n.z,           // x y z
        upperWallTexX, upperWallTexY + 1/TEXTURE_ATLAS_DIM, // u v
        i,                                                  // node #
        isSelected,                                         // selected
        SurfaceType.UPPER_WALL,                             // surface type
        n.x, sector.ceilHeight, n.z,
        upperWallTexX, upperWallTexY,
        i,
        isSelected,
        SurfaceType.UPPER_WALL,
        n.next.x, n.neighboringSector.ceilHeight, n.next.z,
        upperWallTexX + 1/TEXTURE_ATLAS_DIM, upperWallTexY + 1/TEXTURE_ATLAS_DIM,
        i,
        isSelected,
        SurfaceType.UPPER_WALL,
        n.next.x, n.neighboringSector.ceilHeight, n.next.z,
        upperWallTexX + 1/TEXTURE_ATLAS_DIM, upperWallTexY + 1/TEXTURE_ATLAS_DIM,
        i,
        isSelected,
        SurfaceType.UPPER_WALL,
        n.x, sector.ceilHeight, n.z,
        upperWallTexX, upperWallTexY,
        i,
        isSelected,
        SurfaceType.UPPER_WALL,
        n.next.x, sector.ceilHeight, n.next.z,
        upperWallTexX + 1/TEXTURE_ATLAS_DIM, upperWallTexY,
        i,
        isSelected,
        SurfaceType.UPPER_WALL,
      ];

      return lowerWallVerts.concat(upperWallVerts);
    } else {
      return [
        n.x, sector.ceilHeight, n.z,  // x y z
        texX, texY,                   // u v
        i,                            // node #
        isSelected,                   // selected
        SurfaceType.WALL,             // surface type
        n.x, sector.floorHeight, n.z,
        texX, texY + 1/TEXTURE_ATLAS_DIM,
        i,
        isSelected,
        SurfaceType.WALL,
        n.next.x, sector.ceilHeight, n.next.z,
        texX + 1/TEXTURE_ATLAS_DIM, texY,
        i,
        isSelected,
        SurfaceType.WALL,
        n.next.x, sector.ceilHeight, n.next.z,
        texX + 1/TEXTURE_ATLAS_DIM, texY,
        i,
        isSelected,
        SurfaceType.WALL,
        n.x, sector.floorHeight, n.z,
        texX, texY + 1/TEXTURE_ATLAS_DIM,
        i,
        isSelected,
        SurfaceType.WALL,
        n.next.x, sector.floorHeight, n.next.z,
        texX + 1/TEXTURE_ATLAS_DIM, texY + 1/TEXTURE_ATLAS_DIM,
        i,
        isSelected,
        SurfaceType.WALL,
      ];
    }
  });
}

export function generateFloorCeilVertsFromSector(sector: Sector): number[] {
  let points = sector.nodes.flatMap(n => [n.x, n.z]);
  let triangles;
  if (sector.childSectors.length > 0) {
    const childSectorPoints = sector.childSectors.flatMap(cs => cs.nodes.flatMap(n => [n.x, n.z]));
    points = points.concat(childSectorPoints);
    const holeIndices = [sector.nodes.length];
    triangles = (earcut(points, holeIndices));
  }
  else
    triangles = earcut(points);
  const verts: number[] = [];
  for (let i = 0; i < 2; i++) {
    for (let j = 0; j < triangles.length; j += 3) {
      if (i == 0) {
        // floor
        verts.push(
          points[triangles[j  ]*2], sector.floorHeight, points[triangles[j  ]*2+1],
          points[triangles[j+1]*2], sector.floorHeight, points[triangles[j+1]*2+1],
          points[triangles[j+2]*2], sector.floorHeight, points[triangles[j+2]*2+1],
        );
      }
      else {
        // ceiling
        verts.push(
          points[triangles[j  ]*2], sector.ceilHeight,  points[triangles[j  ]*2+1],
          points[triangles[j+1]*2], sector.ceilHeight,  points[triangles[j+1]*2+1],
          points[triangles[j+2]*2], sector.ceilHeight,  points[triangles[j+2]*2+1],
        );
      }
    }
  }

  return verts;
}
