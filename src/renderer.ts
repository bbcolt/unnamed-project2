import {Camera} from './camera';
import {mat4} from 'gl-matrix';
import {compileShader, createProgram, Program, Buffer} from './glUtils';
import {wallVertexShaderSource} from './shaders/vsWall';
import {wallFragmentShaderSource} from './shaders/fsWall';
import {floorCeilVertexShaderSource} from './shaders/vsFloorCeil';
import {floorCeilFragmentShaderSource} from './shaders/fsFloorCeil';
import {Sector} from './sector';
import {Surface, pixelToSurface} from './surface';
import {TEXTURE_ATLAS_DIM} from './texture';

export class Renderer {
  frameCount: number;
  projectionMatrix: mat4;
  matrix: mat4;
  wallProg: Program;
  floorCeilProg: Program;
  wallBuffers: Map<Sector, Buffer>;
  floorCeilBuffers: Map<Sector, Buffer>;
  focusedSurface: Surface;

  constructor(gl: WebGL2RenderingContext) {
    this.focusedSurface = new Surface();
    this.focusedSurface.nodeNum = -1;
    this.frameCount = 0;
    this.projectionMatrix = mat4.create();
    const aspect = gl.canvas.clientWidth/gl.canvas.clientHeight;
    const zNear = 0.1;
    const zFar = 10000;
    mat4.perspective(this.projectionMatrix, Math.PI/4, aspect, zNear, zFar);
    this.matrix = mat4.create();

    const wallVertShader = compileShader(gl, wallVertexShaderSource, gl.VERTEX_SHADER);
    const wallFragShader = compileShader(gl, wallFragmentShaderSource, gl.FRAGMENT_SHADER);
    const glWallProg = createProgram(gl, wallVertShader, wallFragShader);

    this.wallProg = new Program(glWallProg);
    this.wallProg.addAttrib(gl, 'a_position', 3, gl.FLOAT);
    this.wallProg.addAttrib(gl, 'a_texcoord', 2, gl.FLOAT);
    this.wallProg.addAttrib(gl, 'a_wallNum', 1, gl.FLOAT);
    this.wallProg.addAttrib(gl, 'a_selected', 1, gl.FLOAT);
    this.wallProg.addAttrib(gl, 'a_surfaceType', 1, gl.FLOAT);
    this.wallProg.addUniform(gl, 'u_matrix', gl.FLOAT_MAT4);
    this.wallProg.addUniform(gl, 'u_time', gl.FLOAT);
    this.wallProg.addUniform(gl, 'u_mid', gl.FLOAT_VEC2);
    this.wallProg.addUniform(gl, 'u_focusedSectorNum', gl.FLOAT);
    this.wallProg.addUniform(gl, 'u_focusedNodeNum', gl.FLOAT);
    this.wallProg.addUniform(gl, 'u_focusedSurfaceType', gl.FLOAT);
    this.wallProg.addUniform(gl, 'u_sectorNum', gl.FLOAT);

    this.wallBuffers = new Map<Sector, Buffer>();

    const floorCeilVertShader = compileShader(gl,
                                              floorCeilVertexShaderSource,
                                              gl.VERTEX_SHADER);
    const floorCeilFragShader = compileShader(gl,
                                              floorCeilFragmentShaderSource,
                                              gl.FRAGMENT_SHADER);
    const glFloorCeilProg = createProgram(gl, floorCeilVertShader, floorCeilFragShader);
    this.floorCeilProg = new Program(glFloorCeilProg);
    this.floorCeilProg.addAttrib(gl, 'a_position', 3, gl.FLOAT);
    this.floorCeilProg.addUniform(gl, 'u_floorTexOffset', gl.FLOAT_VEC2);
    this.floorCeilProg.addUniform(gl, 'u_ceilTexOffset', gl.FLOAT_VEC2);
    this.floorCeilProg.addUniform(gl, 'u_time', gl.FLOAT);
    this.floorCeilProg.addUniform(gl, 'u_selected', gl.FLOAT);
    this.floorCeilProg.addUniform(gl, 'u_matrix', gl.FLOAT_MAT4);
    this.floorCeilProg.addUniform(gl, 'u_mid', gl.FLOAT_VEC2);
    this.floorCeilProg.addUniform(gl, 'u_focusedSectorNum', gl.FLOAT);
    this.floorCeilProg.addUniform(gl, 'u_focusedSurfaceType', gl.FLOAT);
    this.floorCeilProg.addUniform(gl, 'u_sectorNum', gl.FLOAT);
    this.floorCeilProg.addUniform(gl, 'u_floorVertCount', gl.INT);

    this.floorCeilBuffers = new Map<Sector, Buffer>();

    gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
    gl.enable(gl.DEPTH_TEST);
    gl.clearColor(1, 0.5, 0.5, 1);
  }

  updateSectorWallVerts(gl: WebGL2RenderingContext, sector: Sector, verts: number[]) {
    if (!this.wallBuffers.has(sector)) {
      this.wallBuffers.set(sector, new Buffer(gl, this.wallProg));
    }
    const wallBuf = this.wallBuffers.get(sector);

    wallBuf.verts = verts;
    gl.bindBuffer(gl.ARRAY_BUFFER, wallBuf.buffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(verts), gl.STATIC_DRAW);
  }

  updateSectorFloorCeilVerts(gl: WebGL2RenderingContext, sector: Sector, verts: number[]) {
    if (!this.floorCeilBuffers.has(sector)) {
      this.floorCeilBuffers.set(sector, new Buffer(gl, this.floorCeilProg));
    }
    const floorCeilBuf = this.floorCeilBuffers.get(sector);

    floorCeilBuf.verts = verts;
    gl.bindBuffer(gl.ARRAY_BUFFER, floorCeilBuf.buffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(verts), gl.STATIC_DRAW);
  }

  updateViewProjectionMatrix(camera: Camera) {
    mat4.mul(this.matrix, this.projectionMatrix, camera.viewMatrix);
  }

  render(gl: WebGL2RenderingContext, camera: Camera, sectors: Sector[]) { 
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    this.updateViewProjectionMatrix(camera);

    // draw walls
    gl.useProgram(this.wallProg.glProgram);
    this.wallProg.setUniform(gl, 'u_time', this.frameCount);
    this.wallProg.setUniform(gl, 'u_matrix', this.matrix);
    this.wallProg.setUniform(gl, 'u_mid', [gl.canvas.width/2, gl.canvas.height/2]);
    this.wallProg.setUniform(gl, 'u_focusedSectorNum', this.focusedSurface.sectorNum);
    this.wallProg.setUniform(gl, 'u_focusedNodeNum', this.focusedSurface.nodeNum);
    this.wallProg.setUniform(gl, 'u_focusedSurfaceType', this.focusedSurface.type);
    for (const [sector, wallBuf] of this.wallBuffers) {
      this.wallProg.setUniform(gl, 'u_sectorNum', sector.idx);
      gl.bindVertexArray(wallBuf.vao);
      gl.drawArrays(gl.TRIANGLES,
                    0,
                    wallBuf.verts.length / this.wallProg.numComponents);
    }

    // draw floors/ceils
    gl.useProgram(this.floorCeilProg.glProgram);
    this.floorCeilProg.setUniform(gl, 'u_time', this.frameCount);
    this.floorCeilProg.setUniform(gl, 'u_matrix', this.matrix);
    this.floorCeilProg.setUniform(gl, 'u_mid', [gl.canvas.width/2, gl.canvas.height/2]);
    this.floorCeilProg.setUniform(gl, 'u_focusedSectorNum', this.focusedSurface.sectorNum);
    this.floorCeilProg.setUniform(gl, 'u_focusedSurfaceType', this.focusedSurface.type);
    for (const [sector, floorCeilBuf] of this.floorCeilBuffers) {
      const floorTexOffsetX = (sector.floorTexNum / TEXTURE_ATLAS_DIM) % 1;
      const floorTexOffsetY = Math.floor(sector.floorTexNum / TEXTURE_ATLAS_DIM) / TEXTURE_ATLAS_DIM;
      this.floorCeilProg.setUniform(gl, 'u_floorTexOffset', [floorTexOffsetX, floorTexOffsetY]);
      const ceilTexOffsetX = (sector.ceilTexNum / TEXTURE_ATLAS_DIM) % 1;
      const ceilTexOffsetY = Math.floor(sector.ceilTexNum / TEXTURE_ATLAS_DIM) / TEXTURE_ATLAS_DIM;
      this.floorCeilProg.setUniform(gl, 'u_ceilTexOffset', [ceilTexOffsetX, ceilTexOffsetY]);
      const uSelected = sector.isFloorSelected ? 0.0 :
                        sector.isCeilSelected  ? 1.0 :
                                                -1.0;
      this.floorCeilProg.setUniform(gl, 'u_selected', uSelected);
      this.floorCeilProg.setUniform(gl, 'u_sectorNum', sector.idx);
      this.floorCeilProg.setUniform(gl, 'u_floorVertCount',
                                    (floorCeilBuf.verts.length/
                                     this.floorCeilProg.numComponents)/2);
      gl.bindVertexArray(floorCeilBuf.vao);
      gl.drawArrays(gl.TRIANGLES,
                    0,
                    floorCeilBuf.verts.length / this.floorCeilProg.numComponents);
    }
    const pixel = getPixelUnderReticle(gl);
    this.focusedSurface = pixelToSurface(pixel, sectors);

    this.frameCount++;
  }
}

export function getPixelUnderReticle(gl: WebGL2RenderingContext) {
  const pixel = new Uint8Array(4);
  gl.readPixels(Math.floor(gl.canvas.width/2),
                Math.floor(gl.canvas.height/2),
                1,
                1,
                gl.RGBA,
                gl.UNSIGNED_BYTE,
                pixel);
  return pixel;
}
