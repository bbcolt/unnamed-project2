import {Sector} from './sector';

export enum SurfaceType {
  WALL = 0,
  LOWER_WALL = 1,
  UPPER_WALL = 2,
  FLOOR = 3,
  CEIL = 4,
}

export class Surface {
  type: SurfaceType;
  // Should I get rid of the sector reference?
  sector: Sector;
  sectorNum: number;
  nodeNum?: number;
}

/*
 * [ <pixel[0]>,   <pixel[1]>,   <pixel[2]>,   <pixel[3]> ]
 *  surface type    sector #       wall #        unused
 */
export function pixelToSurface(pixel: Uint8Array, sectors: Sector[]): Surface {
  const surface = new Surface();
  surface.type = pixel[0];
  const sectorNum = pixel[1];
  surface.sector = sectors[sectorNum];
  surface.sectorNum = sectorNum;
  if (surface.type == SurfaceType.FLOOR ||
      surface.type == SurfaceType.CEIL) {
    surface.nodeNum = null;
    return surface;
  } else if (surface.type == SurfaceType.WALL      ||
             surface.type == SurfaceType.LOWER_WALL ||
             surface.type == SurfaceType.UPPER_WALL) {
    surface.nodeNum = pixel[2];
    return surface;
  }
  return surface;
}

export function surfaceToDebugString(surface: Surface) {
  let typeStr;
  switch (surface.type) {
    case SurfaceType.WALL:
      typeStr = 'wall';
      break;
    case SurfaceType.LOWER_WALL:
      typeStr = 'lower wall';
      break;
    case SurfaceType.UPPER_WALL:
      typeStr = 'upper wall';
      break;
    case SurfaceType.FLOOR:
      typeStr = 'floor';
      break;
    case SurfaceType.CEIL:
      typeStr = 'ceil';
      break;
    default:
      typeStr = `unknown surface: ${surface.type}`;
  }
  return typeStr;
}
