export function perpDot(v0: number, v1: number, w0: number, w1: number) {
  return v0*w1 - v1*w0;
}

export function intersect(ax: number, ay: number,
                          bx: number, by: number,
                          cx: number, cy: number,
                          dx: number, dy: number): {r: number, s: number} {
  const rNumer = (ay - cy) * (dx - cx) - (ax - cx) * (dy - cy);
  const rDenom = (bx - ax) * (dy - cy) - (by - ay) * (dx - cx);
  const r = rNumer / rDenom;

  const sNumer = (ay - cy) * (bx - ax) - (ax - cx) * (by - ay);
  const sDenom = (bx - ax) * (dy - cy) - (by - ay) * (dx - cx);
  const s = sNumer / sDenom;
  return {r, s};
}
